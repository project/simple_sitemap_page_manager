<?php

namespace Drupal\simple_sitemap_page_manager\Plugin\simple_sitemap\UrlGenerator;

use Drupal\simple_sitemap\Plugin\simple_sitemap\UrlGenerator\EntityUrlGeneratorBase;
use Drupal\simple_sitemap\Plugin\simple_sitemap\SimpleSitemapPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\simple_sitemap\Entity\EntityHelper;
use Drupal\simple_sitemap\Logger;
use Drupal\Core\Url;
use Drupal\simple_sitemap_page_manager\SimpleSitemapPagesManager;
use Drupal\simple_sitemap\Settings;

/**
 * Pages URL generator plugin.
 *
 * @UrlGenerator(
 * id = "pages",
 * label = @Translation("Pages URL generator"),
 * description = @Translation("Generates URLs for Pages."),
 * )
 */
class PagesUrlGenerator extends EntityUrlGeneratorBase {

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * The sitemap pages manager.
   *
   * @var \Drupal\simple_sitemap_page_manager\SimpleSitemapPagesManager
   */
  protected $pageManager;

  /**
   * The sitemap pages manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ViewsUrlGenerator constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\simple_sitemap\Logger $logger
   *   The simple_sitemap.logger service.
   * @param \Drupal\simple_sitemap\Settings $settings
   *   The settings.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\simple_sitemap\Entity\EntityHelper $entity_helper
   *   The simple_sitemap.entity_helper service.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider.
   * @param \Drupal\simple_sitemap_page_manager\SimpleSitemapPagesManager $pages_manager
   *   The sitemap pages manager.
   */
  public function __construct(
  array $configuration,
  $plugin_id,
  $plugin_definition,
  Logger $logger,
  Settings $settings,
  LanguageManagerInterface $language_manager,
  EntityTypeManagerInterface $entity_type_manager,
  EntityHelper $entity_helper,
  RouteProviderInterface $route_provider,
  SimpleSitemapPagesManager $pages_manager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $logger,
      $settings,
      $language_manager,
      $entity_type_manager,
      $entity_helper
    );
    $this->routeProvider = $route_provider;
    $this->entityTypeManager = $entity_type_manager;
    $this->pageManager = $pages_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): SimpleSitemapPluginBase {
    return new static(
    $configuration,
    $plugin_id,
    $plugin_definition,
    $container->get('simple_sitemap.logger'),
    $container->get('simple_sitemap.settings'),
    $container->get('language_manager'),
    $container->get('entity_type.manager'),
    $container->get('simple_sitemap.entity_helper'),
    $container->get('router.route_provider'),
    $container->get('simple_sitemap_page_manager.pages_manager')
    );
  }

  /**
   * Get the necessary information from each page for indexing in the sitemap.
   *
   * @return array
   *   Get required information to index each page.
   */
  public function getDataSets(): array {
    $data_sets = [];

    // Get all panel pages that are set to be indexed.
    $pages = $this->pageManager->getIndexedPages();
    foreach ($pages as $page) {
      // Load the page entity by the page id.
      $page_entity = $this->entityTypeManager->getStorage('page')->load($page->getPage()->id());
      // Get the access condition of each page.
      $access_conditions = $page_entity->getAccessConditions()->getConfiguration();

      // Set the required information for each page.
      $data_sets[] = [
        'page_id' => $page_entity->id(),
        'access_condition' => $access_conditions,
        'path' => $page_entity->getPath(),
        'priority' => $page->getThirdPartySetting('simple_sitemap_page_manager', 'priority'),
        'changefreq' => $page->getThirdPartySetting('simple_sitemap_page_manager', 'changefreq'),
        'entity_id' => $page_entity->id(),
        'display_id' => $page_entity->getEntityTypeId(),
      ];
    }

    return $data_sets;
  }

  /**
   * Process the information of each page for indexing in the sitemap.
   *
   * @return array
   *   Process the required information to index each page.
   */
  protected function processDataSet($data_set): array {
    $url_object = Url::fromUserInput($data_set['path'], ['absolute' => TRUE]);
    $path = $url_object->getInternalPath();
    $path_data = [
      'url' => $url_object,
      'lastmod' => NULL,
      'priority' => $data_set['priority'],
      'changefreq' => $data_set['changefreq'],
      'access_condition' => $data_set['access_condition'],
      'meta' => [
        'path' => $path,
        'entity_info' => [
          'entity_id' => $data_set['entity_id'],
          'display_id' => $data_set['display_id'],
        ],
      ],
    ];
    return $path_data;
  }

  /**
   * Generate alternate url for all possible languages.
   *
   * @param Drupal\Core\Url $url_object
   *   Url object used to genearate variations.
   */
  protected function getAlternateUrlsForAllLanguages(Url $url_object): array {
    $alternate_urls = [];

    // Get all available languages on the system to create alternate urls.
    foreach ($this->languages as $language) {

      if (!isset($this->settings->get('excluded_languages')[$language->getId()]) || $language->isDefault()) {
        $alternate_urls[$language->getId()] = $this->replaceBaseUrlWithCustom($url_object
          ->setOption('language', $language)->toString(), $url_object
        );
      }
    }

    return $alternate_urls;

  }

}
