<?php

namespace Drupal\simple_sitemap_page_manager;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Manages the retrieval of panel pages selected to be indexed in the sitemap.
 *
 * @package Drupal\simple_sitemap_page_manager
 */
class SimpleSitemapPagesManager {
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Contruct.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * Gets all panel pages that are selected to be indexed in the sitemap.
   *
   * @return array
   *   All panel pages that should be indexed in the sitemap.
   */
  public function getIndexedPages() {
    // Load all pages.
    $page_variant = $this->entityTypeManager->getStorage('page_variant')->loadMultiple();
    $pages = [];
    foreach ($page_variant as $page) {
      // Check if the page is set to be indexed or not.
      if (!is_null($page->getThirdPartySetting('simple_sitemap_page_manager', 'include_sitemap'))
      && $page->getThirdPartySetting('simple_sitemap_page_manager', 'include_sitemap') == 1) {
        array_push($pages, $page);
      }
    }

    return $pages;
  }

}
