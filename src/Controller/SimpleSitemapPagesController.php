<?php

namespace Drupal\simple_sitemap_page_manager\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\simple_sitemap\Settings;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\simple_sitemap_page_manager\SimpleSitemapPagesManager;

/**
 * Controller for Simple XML Sitemap Page Manager admin page.
 */
class SimpleSitemapPagesController extends ControllerBase {

  /**
   * The simple_sitemap.settings service.
   *
   * @var \Drupal\simple_sitemap\Settings
   */
  protected $settings;

  /**
   * The sitemap pages manager.
   *
   * @var \Drupal\simple_sitemap_page_manager\SimpleSitemapPagesManager
   */
  protected $pageManager;

  /**
   * SimplesitemapController constructor.
   *
   * @param \Drupal\simple_sitemap_page_manager\SimpleSitemapPagesManager $pages_manager
   *   The sitemap pages manager.
   * @param \Drupal\simple_sitemap\Settings $settings
   *   The settings.
   */
  public function __construct(SimpleSitemapPagesManager $pages_manager, Settings $settings) {
    $this->pageManager = $pages_manager;
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('simple_sitemap_page_manager.pages_manager'),
      $container->get('simple_sitemap.settings')
    );
  }

  /**
   * Builds a listing of indexed pages displays.
   *
   * @return array
   *   The build table for panel pages.
   */
  public function content() {
    // Create the table structure.
    $table = [
      '#type' => 'table',
      '#header' => [
        $this->t('Pages'),
        $this->t('Display'),
        $this->t('Variants'),
        $this->t('Operations'),
      ],
      '#empty' => $this->t('No page displays are set to be indexed yet. <a href="@url">Edit a page.</a>', ['@url' => $GLOBALS['base_url'] . '/admin/structure/page_manager']),
    ];

    // Get all panel pages that are set to be indexed.
    $page_variant = $this->pageManager->getIndexedPages();

    // Fill in the table with information from each panel page.
    $index = 0;
    foreach ($page_variant as $page) {
      $table[$index]['view'] = ['#markup' => $page->getPage()->label()];
      $table[$index]['display'] = ['#markup' => ucfirst($page->getPage()->getEntityTypeId())];
      $table[$index]['variants'] = ['#markup' => $this->settings->get('default_variant')];

      $display_edit_url = Url::fromRoute('entity.page.edit_form',
        ['machine_name' => $page->getPage()->id(), 'step' => 'general']);
      $table[$index]['operations'] = [
        '#type' => 'operations',
        '#links' => [
          'display_edit' => [
            'title' => $this->t('Edit'),
            'url' => $display_edit_url,
          ],
        ],
      ];
      $index += 1;
    }

    // Builds the table with all the panel pages that will be indexed.
    $build['simple_sitemap_page_manager'] = [
      '#title' => $this->t('Indexed page displays'),
      '#type' => 'fieldset',
      'table' => $table,
    ];

    return $build;
  }

}
