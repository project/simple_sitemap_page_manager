# Simple XML sitemap page manager

This module allows the integration of simple_sitemap with panel pages, adding a new tab in the sitemap settings in the CMS, where all panel pages that are configured to be indexed in the sitemap will be listed.

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the following modules:
- [Panels](https://www.drupal.org/project/panels)
- [Simple XML sitemap](https://www.drupal.org/project/simple_sitemap)

## Installation (required, unless a separate INSTALL.md is provided)

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Enable the module at Administration > Extend.
2. Create a new **Page** on *admin/structure/page_manager*.
3. On the creation wizzard, select if the page will or will not be indexed by the simple_sitemap.
4. Index your simplesitemap on *admin/config/search/simplesitemap*.
5. If you want to manage the pages from the sitemap, you can move to */admin/config/search/simplesitemap/pages*.

## Maintainers

- [murilohp](https://www.drupal.org/u/murilohp)
- [gstivanin](https://www.drupal.org/u/gstivanin)
- [rafmagsou](https://www.drupal.org/u/rafmagsou)
- [brunapimenta](https://www.drupal.org/u/brunapimenta)
- [Leticia Gauna](https://www.drupal.org/u/leticia-gauna)
- [rsussulini](https://www.drupal.org/u/rsussulini)
